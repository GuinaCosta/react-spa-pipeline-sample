import React from 'react';
import logo from './assets/Iteris_logo.png';
import TDCHeader from './assets/header.png'
import './App.css';
import BasicForm from './components/molecules/BasicForm'

function App() {
  const hojeFormatado = new Date().toLocaleDateString()

  return (
    <div className="App">
      <header className="App-header">
        <a
          className="App-link"
          href="https://www.linkedin.com/feed/hashtag/?keywords=iterisnotdc"
          target="_blank"
          rel="noopener noreferrer"
        >
          Olá, pessoas! #IterisNoTDC
          <p>Hoje é {hojeFormatado}</p>
        </a>
        <img
          src={logo}
          className="App-logo"
          alt="logo Iteris" />
        <img
          src={TDCHeader}
          alt="Iteris no TDC" />
      </header>
      <BasicForm />
    </div>
  );
}

export default App;
