import React from "react"
import {render, screen, fireEvent, within} from "@testing-library/react"
import BasicForm from './BasicForm'

function renderForm() {
  return render(<BasicForm />);
}

describe("<BasicForm/>", () => {
  it('should display slider and button', async () => {
    // TODO: test something
    renderForm()
    const slider = screen.getByTestId("slider-temperature")
    slider.focus()
    fireEvent.keyDown(slider, { key: 'ArrowLeft' })

    const input = within(slider).getByRole("slider", { hidden: true })
    // @ts-ignore
    expect(input.value).toEqual("25")
  });
})