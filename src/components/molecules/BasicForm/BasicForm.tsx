import  React, { useState } from 'react'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import Slider from '@mui/material/Slider'
import Paper from '@mui/material/Paper'
import {Mark, Typography} from "@mui/material";

const marks: Mark[] = [
  {
    value: 0,
    label: '0°C',
  },
  {
    value: 20,
    label: '20°C',
  },
  {
    value: 37,
    label: '37°C',
  },
  {
    value: 100,
    label: '100°C',
  },
];

function valueText(value: number): string {
  return `${value}°C`;
}

export default function BasicForm(): JSX.Element {
  const [temperature, setTemperature] =  useState<number>(25);

  const handleChange = (event: Event, newTemperature: number | number[]) => {
    setTemperature(newTemperature as number);
  };

  const showTemperature = (): void => {
    alert(`Temperature collected: ${temperature}º`)
  }

  return (
    <Paper sx={{ paddingLeft: 5 }}>
      <Box sx={{ flexGrow: 1 }} border={"aliceblue"}>
        <Grid container spacing={1} data-testid="container-grid">
          <Grid item xs={8}>
            <Typography variant="h2">
              Say how hot is this presentation:
            </Typography>
          </Grid>
          <Grid item xs={8}>
            <Slider
              data-testid="slider-temperature"
              aria-label="Custom temperature"
              defaultValue={25}
              getAriaValueText={valueText}
              step={1}
              valueLabelDisplay="auto"
              marks={marks}
              onChange={handleChange}
            />
            <input data-testid="temperature-value" type="hidden" value={temperature} />
          </Grid>
          <Grid item xs={4}>
            <Button
              data-testid="button-temperature"
              variant="contained"
              onClick={showTemperature}>SET!</Button>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
}